<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\en_US\Company;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $RAW_QUERY = 'ALTER TABLE user AUTO_INCREMENT = 1';
        $conn = $manager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();


        $RAW_QUERY = 'ALTER TABLE task AUTO_INCREMENT = 1';
        $conn = $manager->getConnection();
        $statement = $conn->prepare($RAW_QUERY);
        $statement->execute();

        $fake = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $passHash = $this->encoder->encodePassword($user, 'password');

            $user
                ->setUsername($fake->userName)
                ->setEmail($fake->email)
                ->setPassword($passHash)
                ->addRoles(User::ROLE_USER)
            ;

            $manager->persist($user);

            $fakeCompany = new Company($fake);

            for ($t = 0; $t < rand(10, 20); $t++) {
                $task = (new Task())
                    ->setTitle($fakeCompany->catchPhrase())
                    ->setCreatedAt($fake->dateTimeThisYear())
                    ->setContent($fake->text(320))
                    ->toggle($fake->boolean)
                    ->setUser($fake->boolean ? $user : null);

                $manager->persist($task);

                if ($t % 10 === 0) {
                    $manager->flush();
                }
            }
        }

        $user = new User();
        $passHash = $this->encoder->encodePassword($user, 'admin');

        $user
            ->setUsername('admin')
            ->setEmail($fake->email)
            ->setPassword($passHash)
            ->addRoles(User::ROLE_ADMIN)
            ->addRoles(User::ROLE_USER)
        ;

        $manager->persist($user);

        $user = new User();
        $passHash = $this->encoder->encodePassword($user, 'user');

        $user
            ->setUsername('user')
            ->setEmail($fake->email)
            ->setPassword($passHash)
        ;

        $manager->persist($user);

        $manager->flush();
    }

}
