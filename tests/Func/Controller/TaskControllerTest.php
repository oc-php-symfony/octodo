<?php

namespace App\Tests\Func\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class TaskControllerTest extends WebTestCase
{
    public function testListAction()
    {
        $client = $this->createClient();
        $this->logUser($client, 'user');
        $client->request(Request::METHOD_GET, '/tasks');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testListDoneAction()
    {
        $client = $this->createClient();
        $this->logUser($client, 'user');
        $client->request(Request::METHOD_GET, '/tasks/done');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testCreateAction()
    {
        $client = $this->createClient();
        $this->logUser($client, 'user');
        $client->request(Request::METHOD_GET, '/tasks/create');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testCreateSubmit()
    {
        $client = $this->createClient();
        $user = $this->logUser($client, 'user');
        $crawler = $client->request(Request::METHOD_GET, '/tasks/create');

        $form = $crawler->selectButton('Ajouter')->form([
            'task[title]' => 'test create with owner',
            'task[content]' => 'lorem ipsum dolor sit amet et conspectus'
        ]);

        $client->submit($form);
        $client->followRedirect();

        /** @var TaskRepository $repo */
        $repo = $client->getContainer()->get(TaskRepository::class);

        /** @var Task $task */
        $task = $repo->findOneBy([
            'title' => 'test create with owner',
        ]);

        $this->assertEquals($user->getId(), $task->getUser()->getId());
    }

    public function testDeleteNotOwnFail()
    {
        $client = $this->createClient();
        $this->logUser($client, 'admin');

        /** @var TaskRepository $repo */
        $repo = $client->getContainer()->get(TaskRepository::class);

        /** @var Task $task */
        $task = $repo->findOneBy([
            'title' => 'test create with owner',
        ]);

        $client->request(Request::METHOD_GET, '/tasks/' . $task->getId() . '/delete');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testDeleteOwnSuccess()
    {
        $client = $this->createClient();
        $this->logUser($client, 'user');

        /** @var TaskRepository $repo */
        $repo = $client->getContainer()->get(TaskRepository::class);

        /** @var Task $task */
        $task = $repo->findOneBy([
            'title' => 'test create with owner',
        ]);

        $client->request(Request::METHOD_GET, '/tasks/' . $task->getId() . '/delete');

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        /** @var Task $task */
        $tasks = $repo->findBy([
            'title' => 'test create with owner',
        ]);

        $this->assertEmpty($tasks);
    }

    public function testDeleteAnonFail()
    {
        $client = $this->createClient();
        $this->logUser($client, 'user');

        /** @var TaskRepository $repo */
        $repo = $client->getContainer()->get(TaskRepository::class);

        /** @var Task $task */
        $task = $repo->findOneBy([
            'User' => null,
        ]);

        $client->request(Request::METHOD_GET, '/tasks/' . $task->getId() . '/delete');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testDeleteAnonByAdminSuccess()
    {
        $client = $this->createClient();
        $user = $this->logUser($client, 'admin');

        /** @var TaskRepository $repo */
        $repo = $client->getContainer()->get(TaskRepository::class);

        /** @var Task $task */
        $task = $repo->findOneBy([
            'User' => null,
        ]);

        $crawler = $client->request(Request::METHOD_GET, '/tasks/' . $task->getId() . '/delete');

        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        $tasks = $repo->findBy([
            'title' => $task->getTitle(),
        ]);

        $this->assertEmpty($tasks);


    }

    protected function logUser($client, $username)
    {
        $users = $client->getContainer()->get(UserRepository::class);
        $user = $users->findOneBy([
            'username' => $username
        ]);

        $session = $client->getContainer()->get('session');
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $user;
    }

}