<?php

namespace App\Tests\Func\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DefaultControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $client = $this->createClient();
        $crawler = $client->request(Request::METHOD_GET, '/');
        //No login, redirect
        $this->assertResponseRedirects($crawler->getBaseHref() . "login", 302);
    }

}