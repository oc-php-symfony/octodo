# Documentation

Online versions have a better chance of being up to date, they are available at :

## UML
Use case : https://drive.google.com/file/d/1OVhGt3lhkNE46uto1KqcUPDovomzlrQ2/view?usp=sharing  
Sequence : https://drive.google.com/file/d/1_xtzzY_mSyGgdPGvuw0Ki5XnfIEQ_mIT/view?usp=sharing  
Class : https://drive.google.com/file/d/1QnwwFpjnbGntuGVCygsKKjFd0ci5ZNfV/view?usp=sharing  

## Authentication and authorization
https://docs.google.com/document/d/1Ww7mxE09VM-XYWvVKNSuD9I1oxINvfUztPQjufJZ-js/edit?usp=sharing

## Quality and performance
https://docs.google.com/document/d/1P7BHzvhhsnqDNXieycX1lzuJVzNr3JxIjix7PxDihZE/edit?usp=sharing

## Contribution
Since contribution is required as a .md file it will be up to date in the origin repository
