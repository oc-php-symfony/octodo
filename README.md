 # ToDoList

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/09f34cd4c6c44cc18a7ef42dc76c7cc6)](https://www.codacy.com/gl/DamienAdam/octodo/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=DamienAdam/octodo&amp;utm_campaign=Badge_Grade)

OpenClassrooms project 8 for Backend/Symfony Developer Path
Original code @ https://github.com/saro0h/projet8-TodoList

## Requirements

-   Apache 2.4 or Nginx 1.13
-   Php 7.2
-   MySQL 5.7
-   Composer 1.9
-   Blackfire 1.48

For your convenience, a docker container is available (see installation chapter)

## Installation

### Without docker

-   clone project in your apache or nginx server root directory
-   run `composer install` to install vendors
-   edit DSN in .env to match your db access `DATABASE_URL=mysql://<<user>>:<<password>>@<<db_host>>:<<db_port>>/<<db_name>>`
-   run `doctrine:database:create` to create the database (not needed if you use the docker container) (add `--env=test` if needed)
-   run `doctrine:migrations:migrate` to create database tables (add `--env=test` if needed)
-   run `doctrine:fixtures:load` to load fixtures (add `--env=test` if needed)
-   to run tests `bin/phpunit`, running from IDE recommended

NB: prefix command with 'php bin/console' or 'bin/console' depending on your OS and setup

### With docker
-   clone into docker container, to "todo" folder (https://gitlab.com/DamienAdam/docker-octodo.git)
-   create a .env to specify blackfire vars, or comment blackfire image out.
-   run `sh runner php dev` from container folder
-   run `php bin/console composer install` to install vendors
-   You shouldn't have to edit the .env DSN if you use the docker package provided
-   run `php bin/console doctrine:migrations:migrate` to create database tables (add `--env=test` if needed)
-   run `php bin/console doctrine:fixtures:load` to load fixtures (add `--env=test` if needed)
-   to run tests `php bin/phpunit`, running from IDE recommended

NB: with docker container, phpmyadmin available at 127.0.0.1:8000 for your convenience

## Authentication and authorization process

The Symfony framework operates authentication by itself, although you can control it through classes and configuration
The best way to understand authentication and authorization in Symfony is to RTFM but as it's mandatory here are a few explanations:

When logging in, the login form is submitted to the backend, the user is looked up against the user's list; if found, the submitted password is then encoded and compared to the lookup user.
After this lookup if there is a match, the user is logged in and a session cookie is set up.

The authentication process is performed by Symfony security core Authentication Provider.

### User's storage
Users are stored in the databases in the ...user table they authenticate against their username, the password is encrypted with bcrypt algorithm
For testing, you can see the “user” and “admin” password used in fixtures with their bcrypt values, because the PasswordEncoder is not used at this point.
NB: the PasswordEncoder is a service provided by Symfony.

### Roles and Hierarchy
The application uses two roles “user” and “admin”, an Admin has all the User rights granted.
Symfony uses an Anonymous role for everything else, i.e. unauthenticated users.

### Access Control
Access control operates through config/packages/security.yaml it allows managing authorization for large parts of the application through routes.

### Voters
On top of Symfony voters, the application uses a custom voter to manage access to ressources, in src/Voter, it allows managing access depending on status, ownership or any other rules you have to enforce.

## Collaboration

### If you're part of the projet
-   clone the project on your local repository
-   create your branch like author/feature
-   work in your local branch
-   when your work is ready, create a merge request to 'dev' branch

### If you're not part of the project but the project is public
-   fork from this main repository
-   try to handle on of the open issues
-   write your code and don't forget the tests
-   open a merge request

### Recommended practices
-   Run a code quality to avoid style and bad practice issues (this project uses Codacy)
-   Use Symfony documentation to stick to intended use of the framework
-   Write run and pass tests
-   Try clean code as much as possible (meaningful variable and method names, single responsibility, short methods...)

## Performance
You can chase performance issues with blackfire profiler and/or the embedded symfony profiler, performance issues and possible fix will be tagged 'optimization' in the issues of the project